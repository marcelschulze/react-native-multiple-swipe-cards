'use strict';
import _slice from 'lodash.slice';
import _clamp from 'lodash.clamp';
import _reverse from 'lodash.reverse';

import React, {PropTypes, Component} from 'react';
import {View, Animated, PanResponder, StyleSheet} from 'react-native';

import NoCardsComponent from './NoCardsComponent';
import CardComponent from './CardComponent';
/* ===============================================================================
cards below (with animations) - done
onClickCard - done
buttons
configable (swipeThreshold, velocity, angular, opacity)
dropShadow
controlledComponent - done
imagePrefetch - done
renderNoCards - done
?
animateIn component (fadeIn?)
=============================================================================== */
const SWIPE_THRESHOLD = 120;
class SwipeCardsComponent extends Component {

  constructor(props) {
    super(props);

    // this._renderPassiveContainer = this._renderPassiveContainer.bind(this);
    this.onRelease = this.onRelease.bind(this);
    this.popCard = this.popCard.bind(this);

    this.pan = new Animated.ValueXY();
    this.pan.x.addListener(({value}) => this._valueX = value);
    this.pan.y.addListener(({value}) => this._valueY = value);

    this.responder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gestureState) => true,
      onStartShouldSetPanResponderCapture: (e, gestureState) => true,
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onPanResponderGrant: (e, gestureState) => {
        this.pan.setOffset({x: this.pan.x._value, y: this.pan.y._value});
        this.pan.setValue({x: 0, y: 0});
        this.touchStart = new Date().getTime();
      },
      onPanResponderMove: Animated.event([
        null, {dx: this.pan.x, dy: this.pan.y},
      ]),
      onPanResponderRelease: this.onRelease,
      onPanResponderTerminate: this.onRelease,
    });
  }

  componentWillUnmount() {
    this.pan.x.removeAllListeners();
    this.pan.y.removeAllListeners();
  }

  calcOffsetY(index, y) {
    return y*index;
  }

  calcScale(reversedIndex) {
    return 1-((reversedIndex)*0.02);
  }

  render() {
    const {cards, cardKey, visibleCardCount, renderNoCards, renderCard, offsetY} = this.props;

    if(cards.length === 0) {
      return renderNoCards();
    }

    // +1 because of one hidden card
    const cardStack = _slice(cards, 0, visibleCardCount + 1);
    const count = cardStack.length;

    // reverse card order, because the first card should lay above the other ones
    const reversed = _reverse(cardStack).map((card, i) => {
      // calculate transforms depending on the card index (reversed style)
      const cardOffsetY = this.calcOffsetY(count - i - 1, offsetY);
      const cardScaleX = this.calcScale(count - i - 1);
      // the end position equals the position of the card above (=> reduce index)
      const cardOffsetYEnd = this.calcOffsetY(count - i - 2, offsetY);
      const cardScaleEnd = this.calcScale(count - i - 2);
      let translateY = 0;
      let translateX = 0;
      let scaleX = 1;
      let rotate = '0deg';
      let panHandlers = {};

      if(i === 0 && count > visibleCardCount) {
        /* ===============================================================================
        rearmost card!
        hide it behind the others at first, show it after transforms
        dont hide if there are not enough cards anymore
        =============================================================================== */
        // to hide the card, set the offsetY the samle value as the card above / before
        const _cardOffsetY = this.calcOffsetY(count - 2, offsetY);
        translateY = Animated.add(this.pan.y, this.pan.x).interpolate({
          inputRange: [-120, 0, 120],
          outputRange: [cardOffsetYEnd, _cardOffsetY, cardOffsetYEnd],
          extrapolate: 'clamp',
        });
        scaleX = Animated.add(this.pan.y, this.pan.x).interpolate({
          inputRange: [-120, 0, 120],
          outputRange: [cardScaleEnd, cardScaleX, cardScaleEnd],
          extrapolate: 'clamp',
        });
      } else if(i === count - 1) {
        /* ===============================================================================
        first card!
        add panHandlers and other transforms
        =============================================================================== */
        rotate = this.pan.x.interpolate({
          inputRange: [-20, 0, 20],
          outputRange: ['1deg', '0deg', '-1deg'],
        });
        translateY = this.pan.y;
        translateX = this.pan.x;
        panHandlers = this.responder.panHandlers;
      } else {
        /* ===============================================================================
        cards between first and last!
        =============================================================================== */
        translateY = Animated.add(this.pan.y, this.pan.x).interpolate({
          inputRange: [-120, 0, 120],
          outputRange: [cardOffsetYEnd, cardOffsetY, cardOffsetYEnd],
          extrapolate: 'clamp',
        });
        scaleX = Animated.add(this.pan.y, this.pan.x).interpolate({
          inputRange: [-120, 0, 120],
          outputRange: [cardScaleEnd, cardScaleX, cardScaleEnd],
          extrapolate: 'clamp',
        });
      }

      const transforms = [
        {translateY},
        {translateX},
        {rotate},
        {scaleX},
      ];

      const c = renderCard(card);

      return React.cloneElement(c, {
        ...c.props,
        panHandlers,
        style: {
          ...c.props.style,
          transform: [
            ...transforms,
          ],
        },
      });
    });

    return (
      <View style={s.container} pointerEvents='box-none'>
        {reversed}
      </View>
    );
  }

  popCard() {
    // pop card if out of the view area (e.g. 380px is save)
    if(Math.abs(this._valueX) > 380) {
      this._valueX > 0
        ? this.props.onYup(this.props.cards[0])
        : this.props.onNope(this.props.cards[0]);
      this.pan.setValue({x: 0, y: 0});
    } else {
      requestAnimationFrame(this.popCard);
    }
  }

  onRelease(e, {vx, vy, dx, dy, isPopCard}) {
     // trigger click, even if you moved the card slightly
    if(Math.abs(dx) < 5 && Math.abs(dy) < 5) {
      const touchEnd = new Date().getTime();
      if(touchEnd <= this.touchStart + 300) {
        this.touchStart = 0;
        this.props.onPress(this.props.cards[0]);
      }
    }

    this.pan.flattenOffset();
    let velocity;

    if(vx >= 0) {
      velocity = _clamp(vx, 2, 3);
    } else if(vx < 0) {
      velocity = _clamp(vx * -1, 2, 3) * -1;
    }

    if(isPopCard || Math.abs(this._valueX) > SWIPE_THRESHOLD) {
      requestAnimationFrame(this.popCard);

      Animated.decay(this.pan, {
        velocity: {x: velocity, y: vy},
        deceleration: 0.998,
      }).start();
    } else {
      Animated.spring(this.pan, {
        toValue: {x: 0, y: 0},
        friction: 4,
      }).start();
    }
  }
}

const s = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
  },
});

SwipeCardsComponent.propTypes = {
  onPress: PropTypes.func.isRequired,
  onYup: PropTypes.func.isRequired,
  onNope: PropTypes.func.isRequired,
  style: View.propTypes.style,
  cards: PropTypes.array.isRequired,
  cardKey: PropTypes.string.isRequired,
  visibleCardCount: PropTypes.number.isRequired,
  offsetY: PropTypes.number.isRequired,
  CardComponent: PropTypes.func.isRequired,
  NoCardComponent: PropTypes.func.isRequired,
};

SwipeCardsComponent.defaultProps = {
  onPress: () => false,
  onYup: () => false,
  onNope: () => false,
  style: s.container,
  cards: [],
  cardKey: 'id',
  visibleCardCount: 3,
  offsetY: 6,
  CardComponent: CardComponent,
  NoCardComponent: NoCardsComponent,
  renderCard: (card) => <CardComponent key={card.id} {...card} style={{}} />,
  renderNoCards: () => <NoCardsComponent />,
};

export default SwipeCardsComponent;
